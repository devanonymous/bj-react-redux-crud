import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import {connect} from "react-redux";
import {getPagesCount, changeCurrentPage} from "../../actionCreators/paginatorActions";
import {getAllTasks} from "../../actionCreators/taskTableActions";


class Paginator extends React.Component {
    componentDidMount() {
        this.props.getPagesCount();
    }

    renderPaginationItems = () => {
        const paginationItem = [];
        const {pagesCount} = this.props;
        for (let i = 1; i <= pagesCount; i++) {
            paginationItem.push((
                <PaginationItem active={this.props.currentPage === i} key={i}>
                    <PaginationLink onClick={this.goToPage(i)} href="#">
                        {i}
                    </PaginationLink>
                </PaginationItem>
            ))
        }
        return paginationItem;
    };

    goToPage = page => () => {
        this.props.changeCurrentPage(page);
        this.props.getAllTasks(page);
    };

    goToFirstPage = () => {
        this.props.changeCurrentPage(1);
        this.props.getAllTasks();
    };

    goToPreviousPage = () => {
        const {currentPage} = this.props;
        this.props.changeCurrentPage(currentPage - 1);
        this.props.getAllTasks(currentPage - 1);
    };

    goToNextPage = () => {
        const {currentPage} = this.props;
        this.props.changeCurrentPage(currentPage + 1);
        this.props.getAllTasks(currentPage + 1);
    };

    render() {
        const {currentPage} = this.props;
        return (
            <Pagination aria-label="Page navigation example">
                <PaginationItem disabled={currentPage === 1}>
                    <PaginationLink onClick={this.goToFirstPage} first href="#" />
                </PaginationItem>
                <PaginationItem disabled={currentPage === 1}>
                    <PaginationLink onClick={this.goToPreviousPage} previous href="#" />
                </PaginationItem>

                {this.props.pagesCount && this.renderPaginationItems()}

                <PaginationItem>
                    <PaginationLink onClick={this.goToNextPage} next href="#" />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink last href="#" />
                </PaginationItem>
            </Pagination>
        );
    }
}

const mapStateToProps = state => {
    return {
        pagesCount: state.paginator.pagesCount,
        currentPage: state.paginator.currentPage,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getPagesCount: () => dispatch(getPagesCount()),
        changeCurrentPage: page => dispatch(changeCurrentPage(page)),
        getAllTasks: (page = 1) => dispatch(getAllTasks(page)),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Paginator);