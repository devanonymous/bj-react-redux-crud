import React from 'react';
import TaskTable from "./table";
import Paginator from "./paginator"
import Header from "./auth";

const Layout = (props) => {
    return (
        <>
            <hr/>
            <Header/>
            <hr/>
            <TaskTable/>
            <hr/>
            <Paginator/>
        </>
    )
};
export default Layout