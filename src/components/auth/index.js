import React from 'react';
import {Button, Form, FormGroup, Label, Input} from 'reactstrap';
import {connect} from "react-redux";
import {ADMIN_LOGIN_AND_PASSWORD} from "../../constants/constants";
import {logIn, logOut} from "./../../actionCreators/authActions";

class Header extends React.Component {
    isLoginAndPasswordCorrect = () => {
        return this.username === ADMIN_LOGIN_AND_PASSWORD.login && this.password === ADMIN_LOGIN_AND_PASSWORD.password;
    };

    onSubmit = (e) => {
        e.preventDefault();
        if (!this.password || !this.username) {
            alert('Все поля должны быть заполнены')
        } else {
            if (this.isLoginAndPasswordCorrect()) {
                this.props.logIn();
            } else {
                alert('Неверный логин и/или пароль ' + this.login + ' ' + this.password)
            }
        }
    };

    handleFieldChange = field => e => {
        switch (field) {
            case "username":
                this.username = e.target.value;
                break;
            case "password":
                this.password = e.target.value;
                break;
        }
    };

    render() {
        if (!this.props.isLogin) {
            return (
                <Form inline onSubmit={this.onSubmit}>
                    <FormGroup>
                        <Label for="username" hidden>username</Label>
                        <Input onChange={this.handleFieldChange("username")} type="text" name="username" id="username"
                               placeholder="username"/>
                    </FormGroup>
                    {' '}
                    <FormGroup>
                        <Label for="examplePassword" hidden>Password</Label>
                        <Input onChange={this.handleFieldChange("password")} type="password" name="password"
                               id="password" placeholder="Password"/>
                    </FormGroup>
                    {' '}
                    <Button>Submit</Button>
                </Form>
            )
        } else {
            return (
                <Button onClick={this.props.logOut}>Logout</Button>
            )
        }
    }
}

const mapStateToProps = state => {
    return {
        isLogin: state.auth.isLogin,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        logIn: () => dispatch(logIn()),
        logOut: () => dispatch(logOut()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);