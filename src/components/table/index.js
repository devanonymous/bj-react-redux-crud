import React from 'react';
import {Table} from 'reactstrap';
import {connect} from "react-redux";
import {Button, Form, FormGroup, Label, Input} from 'reactstrap';

import {getAllTasks, setSorting, addTask} from "../../actionCreators/taskTableActions";
import TableRow from "./TableRow";



class TaskTable extends React.Component {
    componentDidMount() {
        this.props.getAllTasks();
    }

    renderTableRows = () => {
        console.log(this.props, 'props');
        const {tasks} = this.props.tasks.message;
        return tasks.map((task, index) => (
            <TableRow key={index} task={task} isLogin={this.props.isLogin}/>
        ))
    };

    changeSorting = (sortBy) => () => {
        if (sortBy === this.props.sortBy) {
            this.props.setSorting(null);
            this.props.getAllTasks(this.props.currentPage, null);
        } else {
            this.props.setSorting(sortBy);
            this.props.getAllTasks(this.props.currentPage, sortBy);
        }


    };

    handleFieldChange = field => e => {
        switch (field) {
            case "username":
                this.username = e.target.value;
                break;
            case "email":
                this.email = e.target.value;
                break;
            case "text":
                this.text = e.target.value;
                break;
        }
    };

    onSubmit = (e) => {
        e.preventDefault();
        if (!this.username || !this.email || !this.text ) {
            alert('Все поля должны быть заполнены')
        } else {
            this.props.addTask({username: this.username, email:this.email, text: this.text})
        }
    };

    render() {
        return (
            <>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><a onClick={this.changeSorting("username")} href="#">UserName</a></th>
                        <th><a onClick={this.changeSorting("email")} href="#">Email</a></th>
                        <th>Text</th>
                        <th><a onClick={this.changeSorting("status")} href="#">Status</a></th>
                        <th>Image</th>
                    </tr>
                    </thead>
                    <tbody>

                    {this.props.tasks && this.renderTableRows()}

                    </tbody>
                </Table>

                <Form onSubmit={this.onSubmit} inline>
                    <FormGroup>
                        <Label for="username" hidden>username</Label>
                        <Input onChange={this.handleFieldChange("username")} type="text" name="username" id="username"
                               placeholder="Username"/>
                    </FormGroup>
                    {' '}
                    <FormGroup>
                        <Label for="email" hidden>email</Label>
                        <Input onChange={this.handleFieldChange("email")} type="email" name="email" id="email"
                               placeholder="email"/>
                    </FormGroup>
                    {' '}
                    <FormGroup>
                        <Label for="text" hidden>text</Label>
                        <Input onChange={this.handleFieldChange("text")} type="text" name="text" id="text"
                               placeholder="text"/>
                    </FormGroup>
                    {' '}
                    <Button>Submit</Button>
                </Form>
            </>
        )
    }
}

const mapStateToProps = state => {
    console.log('mapStateToProps');
    return {
        tasks: state.taskTable.tasks,
        currentPage: state.paginator.currentPage,
        sortBy: state.taskTable.sortBy,
        isLogin: state.auth.isLogin,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getAllTasks: (page = 1, sortBy = null) => dispatch(getAllTasks(page, sortBy)),
        setSorting: sortBy => dispatch(setSorting(sortBy)),
        addTask: task => dispatch(addTask(task))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(TaskTable);