import {Button} from "reactstrap";
import React from "react";
import {changeStatus, changeText} from "../../actionCreators/taskTableActions";
import {connect} from "react-redux";


class TableRow extends React.Component {
    changeTaskStatusHandler = (e) => {
        if (e.target.checked) {
            this.props.changeStatus(this.props.task.id, 10)
        } else {
            this.props.changeStatus(this.props.task.id, 0)
        }
    };

    handleTextChange = (e) => {
        this.text = e.target.value;
    };

    sendText = () => {
      if (!this.text) {
          alert('Поле не должно быть пустым')
      } else {
          this.props.changeText(this.props.task.id, this.text);
      }
    };

    render() {
        const {isLogin, task} = this.props;
        if (isLogin) {
            return (
                <tr>
                    <th scope="row">{task.id}</th>
                    <td>{task.username}</td>
                    <td>{task.email}</td>
                    <td><input key={'text'+task.id} type="text" onChange={this.handleTextChange} defaultValue={task.text}/><Button onClick={this.sendText}>Subm</Button></td>
                    <td>
                        <input key={task.id} type="checkbox" onChange={this.changeTaskStatusHandler}
                               defaultChecked={task.status === 10}/>
                               <span>{task.status}</span>
                    </td>
                    <td><img height={50} width={50} src={task.image_path} alt={"img"}/></td>
                </tr>
            )
        } else {
            return (
                <tr>
                    <th scope="row">{task.id}</th>
                    <td>{task.username}</td>
                    <td>{task.email}</td>
                    <td>{task.text}</td>
                    <td>
                        <span>{task.status}</span>
                    </td>
                    <td><img height={50} width={50} src={task.image_path} alt={"img"}/></td>
                </tr>
            )
        }
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeStatus: (id,status) => dispatch(changeStatus(id, status)),
        changeText: (id, text) => dispatch(changeText(id,text))
    }
};

export default connect(null, mapDispatchToProps)(TableRow);