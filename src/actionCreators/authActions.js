import {LOGIN, LOGOUT} from "../constants/actionTypes";

export const logIn = () => async dispatch => {
    dispatch({type: LOGIN, payload: true})
};

export const logOut = () => async dispatch => {
    dispatch({type: LOGOUT, payload: false})
};