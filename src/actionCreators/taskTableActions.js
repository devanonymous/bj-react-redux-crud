import md5 from 'md5';
import encode from 'encode-3986';
import {DEVELOPER, SERVER_URL, TOKEN} from "../constants/constants";
import {TASKS_LOADED_SUCCESSFUL, SORTING_CHANGED} from "./../constants/actionTypes"
import {store} from './../store/configureStore'


export const getAllTasks = (page = 1, sortBy = null) => async dispatch => {
    const response = await fetch(`${SERVER_URL}/?developer=${DEVELOPER}&page=${page}&sort_field=${sortBy}`, {
        method: 'GET',
        headers: new Headers({
            "Content-Type": "application/json"
        })
    });

    if (response.ok) {
        dispatch({type: TASKS_LOADED_SUCCESSFUL, payload: await response.json()})
    }
};

export const addTask = task => async dispatch => {
    const form = new FormData();
    form.append("username", task.username);
    form.append("email", task.email);
    form.append("text", task.text);

    const response = await fetch(`${SERVER_URL}/create?developer=${DEVELOPER}`, {
        method: "POST",
        body: form
    });
    if (response.ok) {
        alert("Задача добавлена");
        dispatch(getAllTasks());
    } else {
        alert("Ошибка")
    }
};

export const changeText = (id, text) => async dispatch => {
    text = encode(text);
    const paramsStr = `text=${text}&token=${TOKEN}`;
    const signature = md5(paramsStr);

    console.log(id, decodeURIComponent(text) ,text,signature, paramsStr);

    const form = new FormData();
    form.append("text", text);
    form.append("signature", signature);
    form.append("token", TOKEN);

    const response = await fetch(`${SERVER_URL}edit/${id}?developer=${DEVELOPER}`, {
        method: "POST",
        body: form
    });

    if (response.ok) {
        alert('успех ' + JSON.stringify(await response.json()));
        dispatch(getAllTasks(store.getState().paginator.currentPage));
    } else {
        alert('неудачно ' + await response.json())
    }
};

export const changeStatus = (id, status) => async dispatch => {
    const paramStr = `status=${status}&token=${TOKEN}`;
    const signature = md5(paramStr);

    console.log(id, status, 'changeStatus');

    const form = new FormData();
    form.append("status", status);
    form.append("signature", signature);
    form.append("token", TOKEN);

    const response = await fetch(`${SERVER_URL}edit/${id}?developer=${DEVELOPER}`, {
        method: "POST",
        body: form
    });

    if (response.ok) {
        alert('успех ' + JSON.stringify(await response.json()));
        dispatch(getAllTasks(store.getState().paginator.currentPage));
    } else {
        alert('неудачно ' + await response.json())
    }
};

export const setSorting = (sortBy) => dispatch => {
    dispatch({type: SORTING_CHANGED, payload: sortBy})
};