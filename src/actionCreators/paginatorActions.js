import {DEVELOPER, SERVER_URL} from "../constants/constants";
import {PAGES_COUNT_LOADED_SUCCESSFUL, CHANGE_CURRENT_PAGE} from "../constants/actionTypes";

export const getPagesCount = () => async dispatch => {
    const response = await fetch(`${SERVER_URL}/?developer=${DEVELOPER}`, {
        method: 'GET',
        headers: new Headers({
            "Content-Type":"application/json"
        })
    });
    if (response.ok) {
        const json = await response.json();
        const pageCount =  parseInt(json.message.total_task_count);
        dispatch({type:PAGES_COUNT_LOADED_SUCCESSFUL, payload: Math.ceil(pageCount / 3)})
    }
};

export const changeCurrentPage = page => dispatch => {
    dispatch({type: CHANGE_CURRENT_PAGE, payload: page})
};