import {SORTING_CHANGED, TASKS_LOADED_SUCCESSFUL} from "./../constants/actionTypes"

const initState = {
    sortBy: null
};

export const taskTableReducer = (state = initState, action) => {
    switch (action.type) {
        case TASKS_LOADED_SUCCESSFUL:
            return {...state, tasks: action.payload};
        case SORTING_CHANGED:
            return {...state, sortBy: action.payload};
        default:
            return state;
    }
};