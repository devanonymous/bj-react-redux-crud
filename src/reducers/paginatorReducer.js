import {PAGES_COUNT_LOADED_SUCCESSFUL, CHANGE_CURRENT_PAGE} from "./../constants/actionTypes"

const initialState = {
    pagesCount: 0,
    currentPage: 1
};

export const paginatorReducer = (state = initialState, action) => {
    switch (action.type) {
        case PAGES_COUNT_LOADED_SUCCESSFUL:
            return {...state, pagesCount: action.payload};
        case CHANGE_CURRENT_PAGE:
            return {...state, currentPage: action.payload};

        default:
            return state;
    }
};