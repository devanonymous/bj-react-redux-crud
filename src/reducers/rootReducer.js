import { combineReducers } from "redux";
import {taskTableReducer} from "./taskTableReducer";
import {paginatorReducer} from "./paginatorReducer";
import {authReducer} from "./authReducer"

export const rootReducer = combineReducers({
    taskTable: taskTableReducer,
    paginator: paginatorReducer,
    auth: authReducer
});